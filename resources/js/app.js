$(document).ready( function() {

//  $('.product-wrap').each(function(){
//     var elemTopString = $(this).css('top');
//     var elemShorter = elemTopString.split('p')[0];
//     var output = parseInt(elemShorter.split(/[- ]+/).pop());
//     var rightHeight = $(this).find('.right-wrap').height();
//     thisH = output + rightHeight - 200;
//     if (thisH > maxHeight) { maxHeight = thisH; }
//      });
 //   $('.middle').height(maxHeight);



  if( $('.site-header').siblings('.site-content').children().is('.first') ){
  $('.site-header .logo').addClass('hide');

  $(window).on("scroll", function() {
      if($(window).scrollTop() > 1) {
          $(".site-header").addClass("head-active");
          $('.site-header .logo').removeClass('hide');
      } else {
          $(".site-header").removeClass("head-active");
          $('.site-header .logo').addClass('hide');
      }
  });
}

//produktu lapa

  if( $('.site-header').siblings('.site-content').children().is('.product-page') ){
    $('.site-header').addClass('products-head-active');
    $('a[href^="#home-products"]').addClass('active-nav');

  }

    if( $('.site-header').siblings('.site-content').children().is('.product-info-page') ){
      $('.site-header').addClass('products-head-active');
      $('a[href^="#home-products"]').addClass('active-nav');

    }

    if( $('.site-header').siblings('.site-content').children().is('.about-us-page') ){
      $('.site-header').addClass('products-head-active');
      $('a[href^="#about-us"]').addClass('active-nav');
    }

  $('.cc-link').remove();
  $('.cc-compliance').addClass('btn-main');
  $('.cc-btn').addClass('h5');



  $('.info-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: "<button type='button' class='slick-prev pull-right icon-arrow-left'></button>",
    nextArrow: "<button type='button' class='slick-next pull-right icon-arrow-right'></button>",
  });

  $('a[href^="#"]').click(function() {
      var hash = $(this).attr('href');
        $('html, body').animate({
          scrollTop:-78 + $(hash).offset().top
        }, 1000);
        $('.nav-a').removeClass('active-nav');
        $(this).addClass('active-nav');
        return false;
    });

  if( $('.site-header').siblings('.site-content').children().is('.first') ){
    $('.site-header').find('.logo').attr('href','#first-section');

      $('.site-header .logo').click(function() {
      var hash = $(this).attr('href');
        $('html, body').animate({
          scrollTop:-78 + $(hash).offset().top
        }, 1000);
        return false;
    });

  }


		$(".calendar").datepicker({
            onChangeMonthYear: function () {
              $('.title-header').remove();
              $('.calendar').prepend("<div class='title-header h4'>ragāru kalendārs</div>");
            },


            onSelect: function (date, dp) {
               updateDatePickerCells();
            },

            onChangeMonthYear: function(month, year, dp) {
                updateDatePickerCells();
            },
            beforeShow: function(elem, dp) { //This is for non-inline datepicker
                updateDatePickerCells();
            },
            onSelect: function(date, inst){
              inst.inline = false;
            },


        });

        updateDatePickerCells();

        function updateDatePickerCells(dp) {

				setTimeout(function () {
					$('.ui-datepicker td').each(function (idx, elem) {

						var $thisDatePicker = $(this);

						var i = 0;

            if(frontpage_tours_calendar_data)
            {

						$.each( frontpage_tours_calendar_data, function( key, value ) {

              if(value)
              {
  							var period_element = 0;

  							var period_length = value.period.length;

  							var period_status_class = "";

  							if(value.status == "active")
  							{
  								period_status_class = "date-active";
  							} else {
  								period_status_class = "date-passive";
  							}

                var period_class = "";
                var date_splited = value.date_from.split("-");

                if(  $thisDatePicker.attr('data-month') == (date_splited[1]-1) && $thisDatePicker.attr('data-year') == date_splited[0] && $thisDatePicker.children('a').text() == date_splited[2].replace(/^0+/, ''))
                {

                    $thisDatePicker.addClass(period_status_class + " " + period_class + " activate-hover-block-" + value.ID);

                    $(".hover-block-" + value.ID).clone().appendTo($thisDatePicker);

                    $(".activate-hover-block-" + value.ID).mouseenter(function(){
                      $(this).find('.hover-block').show();
                    });

                      $(".activate-hover-block-" + value.ID).mouseleave(function(){
                      $(this).find('.hover-block').hide();
                      });
                  }
              }
              });
            }
							/*$.each( value.period, function( key, period_date ) {
								var period_class = "date-first";
								var date_splited = period_date.split("-");



								if(  $thisDatePicker.attr('data-month') == (date_splited[1]-1) && $thisDatePicker.attr('data-year') == date_splited[0] && $thisDatePicker.children('a').text() == date_splited[2])
								{

									if(period_element == 0)
									{
										period_class = "date-first";
									}
									else if ( (period_element+1) == period_length )
									{
										period_class = "date-last";
									} else {
										period_class = "date-period";
									}

									$thisDatePicker.addClass(period_status_class + " " + period_class + " activate-hover-block-" + i);

									$(".hover-block-" + i).clone().appendTo($thisDatePicker);

									$(".activate-hover-block-" + i).mouseenter(function(){
										$(this).find('.hover-block').show();
									});

								    $(".activate-hover-block-" + i).mouseleave(function(){
										$(this).find('.hover-block').hide();
								    });
								}

								period_element++;
							});


							i++;
						});*/

					});




				}, 0);
			}




  $(".date-form").datepicker();

  var dateToday = new Date();

  $(".date-form-past-dates-disabled").datepicker({minDate: dateToday});

  $('#ui-datepicker-div').insertAfter('.input-wrap');

  $('.calendar').prepend("<div class='title-header h4'>ragāru kalendārs</div>");














$('body').on('mouseenter','.content',function(){
  $(this).find('.after-hover').addClass('hover-animate');
  $(this).find('.after-hover').siblings('.info').css({opacity: 1}).animate({opacity: 0}, {duration: 200});
});


$('body').on('mouseleave','.content',function(){
  $(this).find('.after-hover').removeClass('hover-animate');
});



$('.second-modal').hide();

$('.require').click(function(){
  $('.modal').first().show();
});

$('.join').click(function(){
  $('.modal').first().show();
});

$('.close-forms').click(function(){
  $('.modal').hide();
});

$('.close-btn').click(function(){
  $('.modal').hide();
});

$(document).click(function(event) {
  //if you click on anything except the modal itself or the "open modal" link, close the modal
  if (!$(event.target).closest(".modal-container,.require,.join,.ui-datepicker-next,.ui-datepicker-prev").length) {
    $("body").find(".modal").hide();
  }
});

$('.submit-buy').click(function(e){

	var passed_validation = true;

	$( ".required-input" ).each(function()
	{

		if( !$(this).val() )
		{
			$(this).css({ border: '1px solid red'});
			passed_validation = false;
		} else {
			$(this).css({ border: 'initial'});
		}

	});

	if(!$('.submit-buy-form input[name="agree_terms"]').is(":checked"))
	{
		$('.submit-buy-form input[name="agree_terms"]').parent().css({ color: 'red'});
		passed_validation = false;
	} else {
		$('.submit-buy-form input[name="agree_terms"]').parent().css({ color: 'initial'});
	}

	var googlRecaptcha = grecaptcha.getResponse();

	if(googlRecaptcha.length == 0)
	{
		$('.submit-buy-form .captcha .form-error').show();
		passed_validation = false;
	} else {
		$('.submit-buy-form .captcha .form-error').hide();
	}

  var emailValidation = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

  if(!emailValidation.test( $('.submit-buy-form input[name="email"]').val() ))
  {
    $('.submit-buy-form input[name="email"]').css({ border: '1px solid red'});
    passed_validation = false;
  } else {
    if($('.submit-buy-form input[name="email"]').val())
    {
      $('.submit-buy-form input[name="email"]').css({ border: 'initial'});
    }
  }

	if(!passed_validation)
	{
		return false;
	}

	jQuery.ajax({
		url : admin_ajax_url,
		type : 'post',
		data : {
			action : 'submit_product_buy',
			form_data : $('.submit-buy-form').serialize()
		},
		success : function( response ) {
			if(response == 'ok')
			{
				$('.modal').first().hide();
				$('.second-modal').show();
				$('.submit-buy-form').trigger("reset");
				grecaptcha.reset();
			}
		}
	});

	e.preventDefault();
});


$('.submit-tour').click(function(e){

				var passed_validation = true;

				$( ".required-input" ).each(function()
				{

					if( $(this).is('input') )
					{

						if( !$(this).val() )
						{
							$(this).css({ border: '1px solid red'});
							passed_validation = false;
						} else {
							$(this).css({ border: 'initial'});
						}

					}
					else if ( $(this).is('select') )
					{

						if( !$(this).find('option:selected') )
						{
							$(this).css({ border: '1px solid red'});
							passed_validation = false;
						} else {
							$(this).css({ border: 'initial'});
						}

					}

				});

				if(!$('.submit-tour-form input[name="agree_terms"]').is(":checked"))
				{
					$('.submit-tour-form input[name="agree_terms"]').parent().css({ color: 'red'});
					passed_validation = false;
				} else {
					$('.submit-tour-form input[name="agree_terms"]').parent().css({ color: 'initial'});
				}

				var googlRecaptcha = grecaptcha.getResponse();

				if(googlRecaptcha.length == 0)
				{
					$('.submit-tour-form .captcha .form-error').show();
					passed_validation = false;
				} else {
					$('.submit-tour-form .captcha .form-error').hide();
				}

        var emailValidation = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

        if(!emailValidation.test( $('.submit-tour-form input[name="email"]').val() ))
        {
          $('.submit-tour-form input[name="email"]').css({ border: '1px solid red'});
          passed_validation = false;
        } else {

          if($('.submit-tour-form input[name="email"]').val())
          {
            $('.submit-tour-form input[name="email"]').css({ border: 'initial'});
          }

        }

				if(!passed_validation)
				{
					return false;
				}

				jQuery.ajax({
					url : admin_ajax_url,
					type : 'post',
					data : {
						action : 'submit_tour_app',
						form_data : $('.submit-tour-form').serialize()
					},
					success : function( response ) {
						if(response == 'ok')
						{
							$('.modal').first().hide();
							$('.second-modal').show();
							$('.submit-tour-form').trigger("reset");
							grecaptcha.reset();
						}
					}
				});

				e.preventDefault();
		});














//select elements
$('.where-seminari .time-form').SumoSelect();
$('.product-choose').SumoSelect({
  search:true
});
$(".where-seminari .SelectBox").find('i').remove();
$(".product-info-page .SelectBox").find('i').addClass('icon-dropdown');






























  $('.slick-active').last().css('border','none');

  $('.slider').on('afterChange', function(){
    $('.slick-slide').css("border-right","1px solid #DDCAC0")
    $('.slick-active').last().css("border", "none");
  });


  $('.slider').on('afterChange', function(){
    $('.slick-current').prev().css("border-right", "1px solid #F3E9E3");
  });




  /////burger-menu
//Burger menu animation with greensock / gsap
var menu = $(".menu"),
    menuitem1 = $(".menu__item--1"),
    menuitem2 = $(".menu__item--2"),
    menuitem3 = $(".menu__item--3"),
    speed = 0.25;

//instantiate  timeline
var tl = new TimelineLite({paused:true}); //pause default

//collapse menu
tl.to(menuitem1, speed, {top: 20, ease:Quint.easeOut}, "label--1")
  .to(menuitem3, speed, {top: -20, ease:Quint.easeOut}, "label--1")

//rotate all items
  .to([menuitem1, menuitem2, menuitem3], speed, {rotation: -90, ease:Quint.easeOut})

//expand menu
  .to(menuitem1, speed, {left: 10, ease:Quint.easeOut}, "label--2")
  .to(menuitem3, speed, {right: 10, ease:Quint.easeOut}, "label--2");


// play timeline on click, reverse animation if active
menu.click(function() {
    $(this).toggleClass('active');
    $(this).parent('.burger-menu').removeClass('flex-x-c');

    if ( $(this).hasClass("active") ) {
        tl.play();
        $(this).children('.menu__item--1').removeClass('before-menu-click-1');
        $(this).children('.menu__item--3').removeClass('before-menu-click-3');
    }
    else {
        tl.reverse();
        $(this).parent('.burger-menu').addClass('flex-x-c');

            $(this).children('.menu__item--1').addClass('before-menu-click-1');
            $(this).children('.menu__item--3').addClass('before-menu-click-3');
    }

});





AOS.init({
    duration: 1200,
    delay:100
  });









slick_slider();
$(window).resize(slick_slider);



function slick_slider() {
  var wrapper = $(".slider");
  if( $(window).width() < 1200 && $(".slider.slick-initialized").length > 0 ) {
    $(".slider").slick('unslick');
  } else if( $(window).width() >= 1200 && !$(".slider.slick-initialized").length > 0 ) {
    wrapper.slick({
      infinite: false,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: true,
      prevArrow: "<button type='button' class='slick-prev pull-left icon-arrow-left bg-overlay'></button>",
      nextArrow: "<button type='button' class='slick-next pull-right icon-arrow-right bg-overlay'></button>"
    });
  }
}

slick_slider();
$(window).resize(function() {
  slick_slider();
});







});








function positionVideoAbout()  {
  if( $('.bef-sert .video').length > 0 ) {
    var $topBlock = $('.bef-sert .video');
    var $elderBlock = $('.blog-post-reverse');
    var $topBlockHeight = $topBlock.innerHeight();
    var $space = parseInt($topBlockHeight / 2);
    $topBlock.css('margin-top', -$space);
    $elderBlock.css('padding-bottom', $space + 100);
  }
}

function positionSertifikati() {
  if( $('.sertifikati .top-bg').length > 0 ) {
    var $topBlock = $('.sertifikati .top-bg');
    var $aboutUsBlock = $('.about-us');
    var $topBlockHeight = $topBlock.innerHeight();
    var $space = parseInt($topBlockHeight / 2);
    $topBlock.css('margin-top', -$space);
    $aboutUsBlock.css('padding-bottom', $space + 100);
  }
}

function positionSertifikatiAbout() {
  if( $('.sertifikati .top-bg').length > 0 ) {
    var $topBlock = $('.sertifikati .top-bg');
    var $aboutUsBlock = $('.one-post');
    var $topBlockHeight = $topBlock.innerHeight();
    var $space = parseInt($topBlockHeight / 2);
    $topBlock.css('margin-top', -$space);
    $aboutUsBlock.css('padding-bottom', $space + 100);
  }
}







$('body').on('click', '.add-product-wrap', function() {
  $('.product-added').first().clone().appendTo('.product-choice');

  var $addedProduct = $('.product-added').last();

  $addedProduct.find('.daudzums').attr('value','1').text('1');
  $addedProduct.find('.SelectBox').remove();
  $addedProduct.find('.time-form').insertAfter($('.product-added').last().find('.top').last());
  $addedProduct.find('.SumoSelect').last().remove();

  $('.product-choose').SumoSelect({
    search:true
  });
  $(".where-seminari .SelectBox").find('i').remove();
  $(".product-info-page .SelectBox").find('i').addClass('icon-dropdown');
});

$('body').on('click', '.icon-count-plus', function() {
    var $plusEl = $(this);
    var $valueEl = $plusEl.parent().find('.daudzums');
    var $inputEl = $plusEl.parent().find('input[type="hidden"]');
    var $minusEl = $plusEl.parent().find('.icon-count-minus');
    var $value = parseInt($valueEl.attr('value'));
    if($value >= 1) {
      $value = parseInt($value + 1);
      $valueEl.attr('value',$value);
      $valueEl.text($value);
	  $inputEl.val($value);
    }
    if($value > 1) {
      $minusEl.addClass('icon-change-color');
    }
    return false;
 });

$('body').on('click', '.icon-count-minus', function() {
  var $minusEl = $(this);
  var $valueEl = $minusEl.parent().find('.daudzums');
  var $inputEl = $minusEl.parent().find('input[type="hidden"]');
  var $plusEl = $minusEl.parent().find('.icon-count-plus');
  var $value = parseInt($valueEl.attr('value'));
  if($value > 1){
    $value = parseInt($value - 1);
    $valueEl.attr('value',$value);
    $valueEl.text($value);
	$inputEl.val($value);
  }
  if($value < 2){
    $minusEl.removeClass('icon-change-color');
  }
  return false;
});

////////////////////////RESPONSIVE FUNCTIONS//////////////////
///////////////////////////////////////////////////
/////////////////////////////////////////////

function addClass1200(){
  if($(window).width() < 1201){
  $('.home-products .slider').addClass('row');
  $('.home-products .slider').addClass('m-x-0');
  $('.burger-menu').removeClass('hide');
  }else{
    $('.home-products .slider').removeClass('row');
    $('.home-products .slider').removeClass('m-x-0');
    $('.burger-menu').addClass('hide');
  }
}

function addClass700(){
  if($(window).width() < 768){
    $('.text-area .text').addClass('flex-x-c');
  }else{
    $('.text-area .text').removeClass('flex-x-c');
  }
}

function addInAside(){
  if($(window).width() < 1221){
    $('.site-header .first-ul').prependTo('.burger-menu .burger-side');
    $('.site-header .second-ul').prependTo('.burger-menu .burger-side');
  }else{
    $('.site-header .first-ul').appendTo('.nav');
    $('.site-header .second-ul').appendTo('.languages');
  }
}


var biggestMiddle = 0;
var biggestWrap = 0;
var biggestWrapTop = 0;
var biggestMiddle1st = 0;

function middleHeight() {

  var $first = $('.first');
  var $top = $first.find('.top');
  var $middle = $first.find('.middle');
  var $bottom = $first.find('.bottom');
  var $productWrap = $first.find('.product-wrap');
  var $windowHeight = $(window).height();
  var $topHeight = $top.outerHeight();

  // .middle height to fill the screen vertically
  var $middleHeightScreen = parseFloat($windowHeight - $topHeight - $bottom.outerHeight());
  $middle.css('height', $middleHeightScreen);
  //console.log('middle height to fill the screen vertically: '+$middleHeightScreen);

  // .middle height according to heighest .product-wrap and it's position
  $productWrap.removeClass('hide');
  $productWrap.css('height','auto');

  $productWrap.each(function() {
    var $this = $(this);
    $wrapHeight = $this.outerHeight();
    $wrapTopFromMiddle = parseFloat($this.offset().top - $topHeight);
    $middleHeightProducts = parseFloat($wrapTopFromMiddle + $wrapHeight);

    if ($middleHeightProducts > biggestMiddle) {
      biggestMiddle = $middleHeightProducts;
      biggestWrap = $wrapHeight;
      biggestWrapTop = $wrapTopFromMiddle;
    }
  });

  //console.log('middle height to according to max product wrap: '+biggestMiddle);
  //console.log('biggest wrap height: '+biggestWrap);
  //console.log('biggest wrap top position from .middle: '+biggestWrapTop);

  if (biggestMiddle > $middleHeightScreen) {
    $middle.css('height', biggestMiddle);
    //console.log('middle height set according to max product wrap (1st time): '+biggestMiddle);
    biggestMiddle1st = biggestMiddle;

    $productWrap.each(function() {
      var $this = $(this);
      $wrapHeight = $this.outerHeight();
      $wrapTopFromMiddle = parseFloat($this.offset().top - $topHeight);
      $middleHeightProducts = parseFloat($wrapTopFromMiddle + $wrapHeight);

      if ($middleHeightProducts > biggestMiddle1st) {
        biggestMiddle = $middleHeightProducts;
        biggestWrap = $wrapHeight;
        biggestWrapTop = $wrapTopFromMiddle;
        //console.log('middle height reset 2nd time');
      }
    });

    //console.log('middle height to according to max product wrap (2nd time): '+biggestMiddle);
    //console.log('biggest wrap top position from .middle (2nd time): '+biggestWrapTop);

    if (biggestMiddle > biggestMiddle1st) {
      $middle.css('height', biggestMiddle);
      //console.log('middle height according to biggest product wrap and its position (2nd time): '+biggestMiddle);
    }
  } else {
    $middle.css('height', $middleHeightScreen);
    //console.log('middle height set to fill the screen vertically');
  }

  $productWrap.addClass('hide');
  $productWrap.css('height','0');

}




function dots(){
  $('.product-wrap').addClass('hide');
  $('.decor').removeClass("decor-animate");
  $('body').on('click','.product-dot',function(e){
  $(this).siblings('.product-wrap').addClass('hide');

  if($(window).width() < 1201 && $(window).width() > 767){
    $(this).next().find('.icon-exit').remove();
    $('<div class="icon-exit"></div>').prependTo($(this).next().find('.decor'));
    $(this).next().removeClass('hide');
    var elementTitle = $(this).next().find('.title');
    var elementList = $(this).next().find('.decor-list');
      e.preventDefault();

      elementTitle.removeClass("title-animate");
      elementList.removeClass("title-animate");

      void elementList.offsetWidth;
      void elementTitle.offsetWidth;

      elementList.addClass("title-animate");
      elementTitle.addClass("title-animate");

      $(this).next().find('.icon-exit').click(function(){
        $(this).parent().parent().addClass('hide');
      });



  }else if($(window).width() > 1200){
    $(this).next().find('.icon-exit').remove();
    $(this).next().find('title').removeClass('title-res-anim');
    $(this).next().find('decor-list').removeClass('title-res-anim');
    $(this).siblings('.product-wrap').addClass('hide');
    $(this).next().removeClass('hide');
    var element = $(this).next().find('.decor');
    var elementTitle = $(this).next().find('.title');
    var elementList = $(this).next().find('.decor-list');
      e.preventDefault();

      elementTitle.removeClass("title-animate");
      elementList.removeClass("title-animate");
      element.removeClass("decor-animate");

      void element.offsetWidth;
      void elementList.offsetWidth;
      void elementTitle.offsetWidth;

      elementList.addClass("title-animate");
      elementTitle.addClass("title-animate");
      element.addClass("decor-animate");


}else if($(window).width() < 768){
  $(this).next().find('.icon-exit').remove();
  $(this).siblings('.product-wrap').addClass('hide');
  $(this).next().addClass('hide');
}
});



}


positionVideoAbout();
positionSertifikati();
positionSertifikatiAbout();
addClass1200();
addClass700();
addInAside();
dots();
middleHeight();



$(window).resize(function(){
  positionSertifikati();
  positionVideoAbout();
  positionSertifikatiAbout();
  addClass1200();
  addClass700();
  addInAside();
  dots();
  middleHeight();

});

