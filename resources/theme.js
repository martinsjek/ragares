// Vendor resources (normalize.css, jquery and plugins by your choice)
require("./vendor/normalize.min.css");
window.$ = window.jQuery = require("./vendor/jquery-3.3.1.min");
require("./vendor/slick-theme.css");
require("./vendor/slick.css");
require("./vendor/aos.css");
require("./vendor/icons.css");
require("./vendor/jquery-ui.css");
require("./vendor/sumoselect.css");




// Your own CSS files
require("./scss/style.scss");
require("./scss/main.scss");


// Your own javascript files
require("./js/app.js");
require("./js/main.js");
require("./js/slick.js");
require("./js/jquery-ui.js");
require("./js/jquery.sumoselect.js");
require("./js/tweenmax.js");


